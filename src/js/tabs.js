export default function (tabs) {
    let tab = document.querySelectorAll('.fantasies-list__tab'),
        info = document.querySelector('.fantasies-list'),
        tabContent = document.querySelectorAll('.fantasies-tabcontent'),
        fantasies = document.querySelector('.fantasies');

    function hideTabContent(a) {
        for (let i = a; i < tabContent.length; i++) {
            tabContent[i].classList.remove('show');
            tabContent[i].classList.add('hide');
        }

        for (let i = a; i < tab.length; i++) {
            tab[i].classList.remove('active');
            tab[i].classList.add('no-active');
            fantasies.classList.remove('fantasies-' + i);
        }
    }
    hideTabContent(1);

    function showTabContent(b) {
        if (tabContent[b].classList.contains('hide')) {
            tabContent[b].classList.remove('hide');
            tabContent[b].classList.add('show');
        }

        if (tab[b].classList.contains('no-active')) {
            tab[b].classList.remove('no-active');
            tab[b].classList.add('active');
        }
    }

    info.addEventListener('click', function (event) {
        let target = event.target;
        
        if (target && target.classList.contains('fantasies-list__tab')) {
            for (let i = 0; i < tab.length; i++) {
                if (target == tab[i]) {
                    hideTabContent(0);
                    showTabContent(i);
                    fantasies.classList.add('fantasies-' + i);
                    break;
                } 
                
            }
        }
    });



}