import Swiper from 'swiper';

export default function (slider) {


  // configure Swiper to use modules


  let menu = ['Rostov-on-Don, Admiral', 'Sochi Thieves', 'Rostov-on-Don Patriotic'];

  let parentSlider = new Swiper('.swiper-container', {
    // Optional parameters
    effect: 'fade',
    navigation: {
      nextEl: '.swiper-next',
      prevEl: '.swiper-prev',
    },
    
    pagination: {
      el: '.swiper-pagination.pagination__bottom, .swiper-pagination.pagination__top',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (menu[index]) + '</span>';
      },
    },


  });



  let childSlider = new Swiper('.projects-item__right', {
    slidesPerView: 1.25,
    spaceBetween: 20,
    centeredSlides: false,
    loop: true,
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
    },
    breakpoints: {
      992: {
        slidesPerView: 1,
        spaceBetween: 20,
        centeredSlides: false,
        loop: true,
        // Navigation arrows
        navigation: {
          prevEl: '.swiper-button-prev',
          nextEl: '.swiper-button-next',
        },
      },
      720: {
        slidesPerView: 1,
        spaceBetween: 15,
        navigation: {
          prevEl: '.swiper-button-prev',
          nextEl: '.swiper-button-next',
        },
      },

    }
  });

}