export default function (mobileMenu) {

    const navbarBurger = document.querySelector('.navbar__burger');
    const navbarMenu = document.querySelector('.navbar__menu');
    const back = document.querySelector('body');
    const navbarList = document.querySelector('.navbar__menu ul');
    const header = document.querySelector('.header');

    navbarBurger.addEventListener('click', function () {
        navbarBurger.classList.toggle('active');
        navbarMenu.classList.toggle('active');
        back.classList.toggle('lock');
        header.classList.toggle('fixed');
    })

    navbarBurger.addEventListener('click', function () {
        navbarList.classList.remove('active');
        back.classList.toggle('lock');
    })



}