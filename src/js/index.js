// import 'core-js';
import '../scss/style.scss';
import '../img/broadcast.svg';
import '../img/slider-right.svg';
import '../img/slider-left.svg';
import '../img/slider-next.svg';
import '../img/projects-bg.svg';
import '../img/repair.svg';
import '../img/repair-1.jpg';
import '../img/repair-2.jpg';
import '../img/repair-3.jpg';
import '../img/pig.svg';
import '../img/team.svg';
import '../img/team-icon-1.svg';
import '../img/team-icon-2.svg';
import '../img/team-icon-3.svg';
import '../img/team-2.jpg';
import '../img/team-3.jpg';
import '../img/team-4.jpg';
import '../img/clients.svg';
import '../img/clients-1.jpg';
import '../img/clients-2.jpg';
import '../img/clients-3.jpg';

import '../img/slider-1.jpg';
import '../img/slider-2.jpg';
import '../img/fantasies-1.jpg';
import '../img/fantasies-2.jpg';
import '../img/fantasies-3.jpg';
import '../img/fantasies-4.jpg';
import '../img/fantasies-5.jpg';
import '../img/fantasies-6.jpg';



import mobileMenu from './mobile-menu.js';
import slider from './slider.js';
import tabs from './tabs.js';


slider();
mobileMenu();
tabs();
